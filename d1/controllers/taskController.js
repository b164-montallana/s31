const Task = require('../models/task');


//Controller function for getting all the tasks

module.exports.getAllTasks = () => {
    return Task.find({}).then(result => {
        return result;
    })
}


//creating a task
module.exports.createTask = (requestBody)=> {

    //Create object
    let newTask = new Task ({
        name: requestBody.name
    })
    return newTask.save().then((task, error) => {
        if(error) {
            console.log(error);
            return false;
        } else {
            return task;
        }
    })
}

//Deleting a task
module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
		if(err){
			console.log(err);
			return false;
		} else {
			return removedTask;
		}
	})
}

//update task 

module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(error);
			return false;
		} 

		result.name = newContent.name; 

		return result.save().then((updatedTask, saveErr) => {
			if(saveErr){
				console.log(saveErr);
				return false;
			} else {
				return updatedTask;
			}
		})

	})
}

//get specific task by Id

module.exports.getTasksId = (taskId) => {
    return Task.findById(taskId).then(result => {
        return result
    })
}

//
module.exports.updateTaskById = (taskId) => {
    return Task.findById(taskId).then((result, error) => {
        if(error) {
            console.log(error);
            return error;
        }

        result.status = "complete"

        return result.save().then((updateTask, saveErr) => {
            if(saveErr){
                console.log(saveErr);
                return false;
            } else {
                return updateTask;
            }
        })
    })
}