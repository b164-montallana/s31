const express = require("express");
const router = express.Router();

const TaskController = require('../controllers/taskController');

//Get all task
router.get("/", (req, res) => {
    TaskController.getAllTasks().then( resultFromcontroller => res.send(resultFromcontroller))
})

//Creating a task
router.post("/", (req, res) => {
    TaskController.createTask(req.body).then(result => res.send(result));
})

//delete a task
router.delete("/:id", (req, res) => {
	TaskController.deleteTask(req.params.id).then(result => res.send(result));
})

//update a task
router.put("/:id", (req, res) => {
    TaskController.updateTask(req.params.id, req.body).then(result => res.send(
        result ));
})

//Get specific task
router.get("/:id", (req, res) => {
    TaskController.getTasksId(req.params.id).then( result => res.send(result));
})

//change status
router.put("/:id/complete", (req, res) => {
    TaskController.updateTaskById(req.params.id).then(result => res.send(
        result
    ));
})


module.exports = router;